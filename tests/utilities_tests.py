import pytest
from utilities import split


def test_split_base():
    assert split('Hello world!', ' ') == ['Hello', 'World!']


def test_split_no_slices():
    assert split('Hello world!', '$') == ['Hello World!']


@pytest.mark.parametrize("text, separator",
                         [
                             (1, ' '),
                             ('Test text', 2),
                             (123, 3.1),
                             (None, ' '),
                             ('Text', None)
                         ])
def test_split_exception(text, separator):
    with pytest.raises(Exception):
        split()
